
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import utfpr.ct.dainf.if62c.pratica.Jogador;
import utfpr.ct.dainf.if62c.pratica.Time;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * 
 * Template de projeto de programa Java usando Maven.
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
public class Pratica61 {
    public static void main(String[] args) {
        Time time1 = new Time();
        Time time2 = new Time();
        boolean t1, t2;
        time1.addJogador("Goleiro", 12, "Julio Cesar");
        time1.addJogador("Zagueiro", 4, "David Luiz");
        time1.addJogador("Atacante", 10, "Neymar");
        time2.addJogador("Goleiro", 12, "Taffarel");
        time2.addJogador("Zagueiro", 4, "Aldair");
        time2.addJogador("Atacante", 10, "Romário");
        
        Set keysettime1=time1.jogadores.keySet();
        Set keysettime2=time2.jogadores.keySet();
//      time1.getJogadores();
  //      time2.getJogadores();

        Iterator<Set> chave1 = keysettime1.iterator();
        Iterator<Set> chave2 = keysettime2.iterator();
  
        System.out.println("Escalação :");

//exemplo do Bogado

        Set<Map.Entry<String, Jogador>> entries = time1.jogadores.entrySet();
        //Set<Map.Entry<String, Jogador>> entries2 = time2.jogadores.entrySet();
        if(chave1.hasNext()){            
            System.out.println("Posição         Time 1              Time 2");
          for (Map.Entry<String, Jogador> entry: entries) {
            System.out.println("               "+time1.jogadores.get(chave1.next())+"  "+time2.jogadores.get(chave2.next()));
            //System.out.println(time22.get(chave11.next()));
          }
        }

    }
}
